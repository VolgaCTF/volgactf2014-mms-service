# --- !Ups

CREATE TABLE Messages (
    id bigint(20) NOT NULL,
    user_id bigint(20) NOT NULL,
    title VARCHAR(255),
    text TEXT,
    posted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id,user_id)
);

# --- !Downs

DROP TABLE User;