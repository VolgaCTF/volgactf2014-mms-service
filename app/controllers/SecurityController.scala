package controllers

import play.api.mvc.{Action, Controller}
import play.api.data._
import play.api.data.Forms._
import models.{LoginError, User}
import play.api.{Routes, Logger}

object SecurityController extends Controller{

  val loginForm = Form(
    tuple(
      "username" -> nonEmptyText(maxLength = 255),
      "password" -> nonEmptyText(minLength = 3, maxLength = 255)
    )
  )

  def login = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }

  def doLogin() = Action { implicit request =>
    var hasErrors = false
    var error:LoginError = null
    loginForm.bindFromRequest().fold(formWithErrors =>
      BadRequest(views.html.login(formWithErrors)),

      loginPair =>{
        try {
           User.authenticate(loginPair._1, loginPair._2).fold(hasErrors=false)(logError => {error=logError;hasErrors=true;true})
        } catch {
          case e:Exception => Logger.error("login exception",e)
          case e:Error => Logger.error("Login error",e)
        }
        if (!hasErrors){
          Redirect(routes.Application.index()).withSession("username" -> loginPair._1)
        }
        else{
          Redirect(routes.SecurityController.login()).flashing(("error",error.message))
        }
      }
    )
  }


  def logout = Action { implicit request =>
    Redirect(routes.Application.index()).withNewSession
  }

}
